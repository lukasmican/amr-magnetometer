/*
 * offset_coil_driver.c
 *
 *  Created on: 11. 6. 2021
 *      Author: lukas
 */


#include "offset_coil_driver.h"
#include "main.h"
#include <math.h>






void OFFSET_driver_init(OFFSET_driver_t * offset_driver,
		float ref_resistance,float ref_voltage1, float ref_zero1,
		float ref_voltage2, float ref_zero2,
		uint32_t volatile * volatile cmp_reg_addr1,
		uint32_t volatile * volatile cmp_reg_addr2,
		float Kp, float Ki){

	offset_driver->cmp_reg_addr1 = cmp_reg_addr1;
	offset_driver->cmp_reg_addr2 = cmp_reg_addr2;
	offset_driver->ref_resistance = ref_resistance;
	offset_driver->ref_voltage1 = ref_voltage1;
	offset_driver->ref_zero1 = ref_zero1;
	offset_driver->ref_voltage2 = ref_voltage2;
	offset_driver->ref_zero2 = ref_zero2;
	offset_driver->Kp = Kp;
	offset_driver->Ki = Ki;
	offset_driver->error_sum = 0;

	offset_driver->current_pwm1 = OFFSET_PWM_MIN;
	offset_driver->pwm_table_row1 = 0;
	offset_driver->pwm_table_column1 = 0;
	offset_driver->current_pwm2 = OFFSET_PWM_MIN;
	offset_driver->pwm_table_row2 = 0;
	offset_driver->pwm_table_column2 = 0;

}

void OFFSET_driver_setCurrent(OFFSET_driver_t * offset_driver, float current_mA){
	float w = (2.0*(current_mA*offset_driver->ref_resistance/1000.0 - offset_driver->ref_zero1 + offset_driver->ref_zero2) - offset_driver->ref_voltage1 + offset_driver->ref_voltage2)
					/
			  (offset_driver->ref_voltage1 + offset_driver->ref_voltage2);

	if (w > 1) {
		w = 1;
	} else if (w < -1){
		w = -1;
	}

	uint32_t pwm1 = round(((1.0-w)/2.0) * OFFSET_PWM_PERIOD);
	uint32_t pwm2 = round(((1.0+w)/2.0) * OFFSET_PWM_PERIOD);

	if (pwm1 < OFFSET_PWM_MIN){
		pwm1 = OFFSET_PWM_MIN;
	}
	if (pwm2 < OFFSET_PWM_MIN){
		pwm2 = OFFSET_PWM_MIN;
	}


	offset_driver->current_pwm1 = pwm1 >> OFFSET_PWM_EXT_RES_BITS;
	offset_driver->pwm_table_row1 = pwm1 & (OFFSET_PWM_EXT_RES_LEVELS - 1);
	offset_driver->pwm_table_column1 = 0;

	offset_driver->current_pwm2 = pwm2 >> OFFSET_PWM_EXT_RES_BITS;
	offset_driver->pwm_table_row2 = pwm2 & (OFFSET_PWM_EXT_RES_LEVELS - 1);
	offset_driver->pwm_table_column2 = 0;


	//(offset_driver->cmp_reg_addr1) = pwm1;
	//(offset_driver->cmp_reg_addr2) = pwm2;
}

void OFFSET_driver_setState(OFFSET_driver_t * offset_driver, uint8_t state){
	HAL_GPIO_WritePin(OFFSET_ENABLE_GPIO_Port, OFFSET_ENABLE_Pin, state);
}

void OFFSET_driver_enable(OFFSET_driver_t * offset_driver){
	OFFSET_driver_setState(offset_driver, GPIO_PIN_SET);
}
void OFFSET_driver_disable(OFFSET_driver_t * offset_driver){
	OFFSET_driver_setCurrent(offset_driver, 0);
	OFFSET_driver_setState(offset_driver, GPIO_PIN_RESET);
}

float OFFSET_driver_regulator_iteration(OFFSET_driver_t * offset_driver, float input_value){
	float error = (0 - input_value);	// we are trying to counter the magnetic field
	offset_driver->error_sum += error;

	float regulation = offset_driver->Kp * error + offset_driver->Ki * offset_driver->error_sum;
	if (regulation > 1){
		regulation = 1;
		offset_driver->error_sum -= error;
	} else if (regulation < -1){
		regulation = -1;
		offset_driver->error_sum -= error;
	}

	offset_driver->current_mA = regulation * OFFSET_MAX_CURRENT;
	OFFSET_driver_setCurrent(offset_driver, offset_driver->current_mA);

	return offset_driver->current_mA;
}
