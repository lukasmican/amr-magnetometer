/*
 * dcdc_converter.h
 *
 *  Created on: Apr 29, 2021
 *      Author: lukas
 */

#ifndef INC_DCDC_CONVERTER_H_
#define INC_DCDC_CONVERTER_H_

#include <stdint.h>
#include <stdio.h>
#include "stm32f3xx_hal.h"
#include "print.h"

#define DCDC_PWM_PERIOD 0xFFF

typedef struct {
	float voltage_koef1;
	float voltage_koef2;
	float Kp1;
	float Ki1;
	float Kp2;
	float Ki2;
	float target_voltage;
	float error_sum1;
	float error_sum2;
	uint32_t volatile * volatile cmp_reg_addr;
	uint16_t * adc_reading_addr1;
	uint16_t * adc_reading_addr2;
	float current_voltage1;
	float current_voltage2;
	uint8_t enable_fixed_pwm;
	uint16_t fixed_pwm;
	uint8_t enabled;
} DCDC_regulator_t;


// fills the DCDC_converter_t structure
void DCDC_regulator_init(DCDC_regulator_t * dcdc,
		float voltage_koef1, float voltage_koef2, float Kp1, float Ki1, float Kp2, float Ki2,
		uint32_t volatile * volatile cmp_reg_addr, uint16_t * adc_reading_addr1, uint16_t * adc_reading_addr2, uint8_t enable_fixed_pwm, uint16_t fixed_pwm);

// performs complete regulation cycle - reads input, processes data and sets output
void DCDC_regulator_iteration(DCDC_regulator_t * dcdc);

void DCDC_enable();
void DCDC_disable();

#endif /* INC_DCDC_CONVERTER_H_ */
