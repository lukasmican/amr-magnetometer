/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f3xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdint.h>
#include "dcdc_converter.h"
#include "offset_coil_driver.h"

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_HRTIM_MspPostInit(HRTIM_HandleTypeDef *hhrtim);

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define LED1_Pin GPIO_PIN_3
#define LED1_GPIO_Port GPIOA
#define LED2_Pin GPIO_PIN_4
#define LED2_GPIO_Port GPIOA
#define HBRIDGE_DISABLE_Pin GPIO_PIN_2
#define HBRIDGE_DISABLE_GPIO_Port GPIOB
#define OFFSET_ENABLE_Pin GPIO_PIN_12
#define OFFSET_ENABLE_GPIO_Port GPIOA
/* USER CODE BEGIN Private defines */
UART_HandleTypeDef huart1;


uint16_t ADC_measurements[4];

#define SPI_BUFFER_LENGTH 80
int16_t spi_data[SPI_BUFFER_LENGTH];
int16_t output_spi_data[SPI_BUFFER_LENGTH];

DCDC_regulator_t dcdc1;
DCDC_regulator_t dcdc2;

OFFSET_driver_t offset_driver;

#define ADC1_VOLTAGE_KOEF 3.25/4096.0

#define DCDC1_VOLTAGE_KOEF 223.3/3.37
#define DCDC1_Kp 0.1 //0.1
#define DCDC1_Ki 0.0001 //0.0005


#define DCDC2_VOLTAGE_KOEF 223.3/3.28
#define DCDC2_VOLTAGE_KOEF2 223.3/3.38
#define DCDC2_Kp1 0.01 //0.1
#define DCDC2_Ki1 0.0001 //0.0005
#define DCDC2_Kp2 0.01 //0.1
#define DCDC2_Ki2 0.0001 //0.0005

#define SET_RESET_COIL_VOLTAGE 25	// => 3 A
#define AMR_BRIDGE_VOLTAGE 10



#define CLOSED_LOOP_MODE 1

#define OFFSET_DRV_ref_resistance 300.0
#define OFFSET_DRV_ref_voltage1 3.0
#define OFFSET_DRV_ref_zero1 0.0
#define OFFSET_DRV_ref_voltage2 3.0
#define OFFSET_DRV_ref_zero2 0.0

#define OFFSET_DRV_Kp 0.0000002
#define OFFSET_DRV_Ki 0.0000007


/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
