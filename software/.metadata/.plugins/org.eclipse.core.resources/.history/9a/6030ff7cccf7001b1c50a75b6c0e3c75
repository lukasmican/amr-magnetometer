/*
 * offset_coil_driver.h
 *
 *  Created on: 11. 6. 2021
 *      Author: lukas
 */

#ifndef SRC_OFFSET_COIL_DRIVER_H_
#define SRC_OFFSET_COIL_DRIVER_H_


#include <stdint.h>
#include <stdio.h>
#include "stm32f3xx_hal.h"

#define OFFSET_PWM_EXT_RES_LEVELS 16 // +4bits

static uint8_t PWM_seq[OFFSET_PWM_EXT_RES_LEVELS][OFFSET_PWM_EXT_RES_LEVELS] = {
		{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, 	// sum = 0/16
		{ 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0 }, 	// sum = 1/16
		{ 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1 }, 	// sum = 2/16
		{ 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0 }, 	// sum = 3/16
		{ 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1 }, 	// sum = 4/16
		{ 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0 }, 	// sum = 5/16
		{ 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0 }, 	// sum = 6/16
		{ 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0 }, 	// sum = 7/16
		{ 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1 }, 	// sum = 8/16
		{ 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 1 }, 	// sum = 9/16
		{ 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0 }, 	// sum = 10/16
		{ 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1 }, 	// sum = 11/16
		{ 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1 }, 	// sum = 12/16
		{ 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1 }, 	// sum = 13/16
		{ 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1 },    	// sum = 14/16
		{ 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 } };  	// sum = 15/16



#define OFFSET_PWM_PERIOD 0xFFDF

#define OFFSET_MAX_CURRENT 10 // mA


typedef struct {
	float ref_resistance;
	float ref_voltage1;
	float ref_zero1;
	float ref_voltage2;
	float ref_zero2;
	uint32_t volatile * volatile cmp_reg_addr1;
	uint32_t volatile * volatile cmp_reg_addr2;

	float Kp;
	float Ki;
	float error_sum;
	float current_mA;

	uint16_t current_pwm1;
	uint8_t * pwm_table_row1;
	uint8_t pwm_table_column1;
	uint16_t current_pwm2;
	uint8_t * pwm_table_row2;
	uint8_t pwm_table_column2;
} OFFSET_driver_t;


// fills the DCDC_converter_t structure
void OFFSET_driver_init(OFFSET_driver_t * offset_driver,
		float ref_resistance,float ref_voltage1, float ref_zero1,
		float ref_voltage2, float ref_zero2,
		uint32_t volatile * volatile cmp_reg_addr1,
		uint32_t volatile * volatile cmp_reg_addr2,
		float Kp, float Ki);

void OFFSET_driver_setCurrent(OFFSET_driver_t * offset_driver, float current_mA);
void OFFSET_driver_setState(OFFSET_driver_t * offset_driver, uint8_t state);
void OFFSET_driver_enable(OFFSET_driver_t * offset_driver);
void OFFSET_driver_disable(OFFSET_driver_t * offset_driver);
float OFFSET_driver_regulator_iteration(OFFSET_driver_t * offset_driver, float input_value);

#endif /* SRC_OFFSET_COIL_DRIVER_H_ */
