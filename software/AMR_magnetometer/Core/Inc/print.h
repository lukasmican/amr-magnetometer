/*
 * print.c
 *
 *  Created on: Jun 16, 2021
 *      Author: lukas
 */

#ifndef INC_PRINT_C_
#define INC_PRINT_C_

#include <stdint.h>
uint8_t UART_TxData[600];

int16_t int16_array_to_string(int16_t * int_array, int16_t int_array_len, uint8_t * string);
int16_t int16_frame_to_string(int16_t * int_array, int16_t int_array_len, uint8_t * string);
int32_t int32_array_to_string(int32_t * int_array, int16_t int_array_len, uint8_t * string);

int16_t float_frame_to_string(float * float_array, int16_t float_array_len, uint8_t * string);
#endif /* INC_PRINT_C_ */
