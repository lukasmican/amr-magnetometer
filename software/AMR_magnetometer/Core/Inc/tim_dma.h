/*
 * tim_dma.h
 *
 *  Created on: Jun 17, 2021
 *      Author: lukas
 */

#ifndef INC_TIM_DMA_H_
#define INC_TIM_DMA_H_

#include "stm32f3xx_hal.h"

HAL_StatusTypeDef MY_HAL_TIM_PWM_Start_DMA(TIM_HandleTypeDef *htim, uint32_t Channel, uint32_t *src, uint32_t *dst, uint16_t Length);


#endif /* INC_TIM_DMA_H_ */
