/*
 * print.c
 *
 *  Created on: Jun 16, 2021
 *      Author: lukas
 */

#include <stdint.h>
#include "print.h"


int16_t int16_array_to_string(int16_t * int_array, int16_t int_array_len, uint8_t * string){
	const int16_t number_len = 5;
    int16_t total_number_len = number_len + 2;

	int16_t w = 0;
    int16_t string_pos = 0;
	for (int i=0; i<int_array_len; i++){
		w = int_array[i];
        string_pos = i*total_number_len;
        if (w < 0){
            string[string_pos] = '-';
            w = -w;
        } else {
            string[string_pos] = '+';
        }

		for (int j=number_len; j>0; j--){
			string[string_pos + j] = (w % 10) + '0';
			w = w / 10;
		}
		string[string_pos + number_len+1] = '\n';
	}
	return total_number_len * int_array_len;
}


int16_t int16_frame_to_string(int16_t * int_array, int16_t int_array_len, uint8_t * string){
	const int16_t number_len = 5;
    int16_t total_number_len = number_len + 2;

	int16_t w = 0;
    int16_t string_pos = 0;
	for (int i=0; i<int_array_len; i++){
		w = int_array[i];
        string_pos = i*total_number_len;
        if (w < 0){
            string[string_pos] = '-';
            w = -w;
        } else {
            string[string_pos] = '+';
        }

		for (int j=number_len; j>0; j--){
			string[string_pos + j] = (w % 10) + '0';
			w = w / 10;
		}
		string[string_pos + number_len+1] = ' ';
	}
	string[string_pos + number_len+1] = '\n';
	return total_number_len * int_array_len;
}

int16_t float_frame_to_string(float * float_array, int16_t float_array_len, uint8_t * string){
	const int16_t number_len = 5;
	const int16_t decimals_len = 5;
    int16_t total_number_len = number_len + decimals_len + 1 + 2;

	int32_t w = 0;
    int16_t string_pos = 0;
	for (int i=0; i<float_array_len; i++){
        string_pos = i*total_number_len;
        if (float_array[i] < 0){
        	w = (int32_t)(-float_array[i]);
            string[string_pos] = '-';
        } else {
        	w = (int32_t)(float_array[i]);
            string[string_pos] = '+';
        }

		for (int j=number_len; j>0; j--){
			string[string_pos + j] = (w % 10) + '0';
			w = w / 10;
		}
		string[string_pos + number_len+1] = '.';


		if (float_array[i] < 0){
			w = (int32_t)((-float_array[i]-((int32_t)(-float_array[i]))) * 100000.0);
		} else {
			w = (int32_t)((float_array[i]-((int32_t)(float_array[i]))) * 100000.0);
		}
		for (int j=number_len+decimals_len+1; j>number_len+1; j--){
			string[string_pos + j] = (w % 10) + '0';
			w = w / 10;
		}
		string[string_pos + total_number_len-1] = ' ';
	}
	string[string_pos + total_number_len-1] = '\n';
	return total_number_len * float_array_len;
}


int32_t int32_array_to_string(int32_t * int_array, int16_t int_array_len, uint8_t * string){
	const int16_t number_len = 8;
    int16_t total_number_len = number_len + 2;

	int16_t w = 0;
    int16_t string_pos = 0;
	for (int i=0; i<int_array_len; i++){
		w = int_array[i];
        string_pos = i*total_number_len;
        if (w < 0){
            string[string_pos] = '-';
            w = -w;
        } else {
            string[string_pos] = '+';
        }

		for (int j=number_len; j>0; j--){
			string[string_pos + j] = (w % 10) + '0';
			w = w / 10;
		}
		string[string_pos + number_len+1] = '\n';
	}
	return total_number_len * int_array_len;
}

