/*
 * dcdc_converter.c
 *
 *  Created on: Apr 29, 2021
 *      Author: lukas
 */

#include "dcdc_converter.h"
#include "main.h"



void DCDC_regulator_init(DCDC_regulator_t * dcdc,
		float voltage_koef1, float voltage_koef2, float Kp1, float Ki1, float Kp2, float Ki2,
		uint32_t volatile * volatile cmp_reg_addr, uint16_t * adc_reading_addr1, uint16_t * adc_reading_addr2, uint8_t enable_fixed_pwm, uint16_t fixed_pwm){
	dcdc->voltage_koef1 = voltage_koef1;
	dcdc->voltage_koef2 = voltage_koef2;
	dcdc->Kp1 = Kp1;
	dcdc->Ki1 = Ki1;
	dcdc->Kp2 = Kp2;
	dcdc->Ki2 = Ki2;
	dcdc->target_voltage = 0;
	dcdc->cmp_reg_addr = cmp_reg_addr;
	dcdc->adc_reading_addr1 = adc_reading_addr1;
	dcdc->adc_reading_addr2 = adc_reading_addr2;
	dcdc->error_sum1 = 0;
	dcdc->error_sum2 = 0;
	dcdc->current_voltage1 = 0;
	dcdc->current_voltage2 = 0;
	dcdc->enable_fixed_pwm = enable_fixed_pwm;
	dcdc->fixed_pwm = fixed_pwm;
	dcdc->enabled = 0;
	dcdc->current_regulation = 0;
}

void DCDC_enable(DCDC_regulator_t * dcdc){
	dcdc->enabled = 1;
}
void DCDC_disable(DCDC_regulator_t * dcdc){
	dcdc->enabled = 0;
}

void DCDC_regulator_iteration(DCDC_regulator_t * dcdc){
	int adc_input1, adc_input2;
	float voltage1, voltage2;

	uint16_t pwm = DCDC_PWM_PERIOD + 1;
	if (dcdc->enabled != 0){

		adc_input1 = *(dcdc->adc_reading_addr1);
		voltage1 = adc_input1 * dcdc->voltage_koef1;
		dcdc->current_voltage1 = voltage1;
		if (dcdc->adc_reading_addr2){
			adc_input2 = *(dcdc->adc_reading_addr2);
			voltage2 = adc_input2 * dcdc->voltage_koef2;
			dcdc->current_voltage2 = voltage2;
		}

		if (dcdc->target_voltage != 0){
			if (dcdc->enable_fixed_pwm){
				if (voltage1 <= dcdc->target_voltage*2 && voltage2 <= dcdc->target_voltage){
					pwm = dcdc->fixed_pwm;
				} else {
					pwm = DCDC_PWM_PERIOD + 1;
				}
			} else {

				float voltage2_offset = 0;
				if (dcdc->adc_reading_addr2){
					float error2 = dcdc->target_voltage - voltage2;
					dcdc->error_sum2 += error2;
					voltage2_offset = dcdc->Kp2 * error2 + dcdc->Ki2 * dcdc->error_sum2;
					if (voltage2_offset > dcdc->target_voltage) dcdc->error_sum2 -= error2;
					if (voltage2_offset < 0){
						voltage2_offset = 0;
						dcdc->error_sum2 = 0;
					}
				}


				float error1 = dcdc->target_voltage + voltage2_offset - voltage1;
				dcdc->error_sum1 += error1;



				float regulation = dcdc->Kp1 * error1 + dcdc->Ki1 * dcdc->error_sum1;

				if (regulation > 1){
					regulation = 1;
					dcdc->error_sum1 -= error1;
				} else if (regulation < 0){
					dcdc->error_sum1 = 0;
					regulation = 0;
				}
				dcdc->current_regulation = regulation;




				if (regulation <= 0) {
					pwm = DCDC_PWM_PERIOD + 1;
				} else {
					pwm = DCDC_PWM_PERIOD * (1.0 - regulation);
					if (pwm < DCDC_PWM_PERIOD*0.05){
						pwm = DCDC_PWM_PERIOD*0.05;
					} else if (pwm > DCDC_PWM_PERIOD*0.95){
						pwm = DCDC_PWM_PERIOD*0.95;
					}
				}
			}
		}
	}

	*(dcdc->cmp_reg_addr) = pwm;
}

