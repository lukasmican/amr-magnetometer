EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 12
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	6200 2700 6300 2700
Wire Wire Line
	6400 2700 6300 2700
Connection ~ 6300 2700
Wire Wire Line
	6200 2700 6100 2700
Connection ~ 6200 2700
Wire Wire Line
	6100 5850 6200 5850
Connection ~ 6300 5850
Wire Wire Line
	6300 5850 6400 5850
Connection ~ 6200 5850
Wire Wire Line
	6200 5850 6300 5850
$Comp
L power:GND #PWR?
U 1 1 5FD52757
P 6100 5850
AR Path="/5FD52757" Ref="#PWR?"  Part="1" 
AR Path="/5FD409CD/5FD52757" Ref="#PWR020"  Part="1" 
F 0 "#PWR020" H 6100 5600 50  0001 C CNN
F 1 "GND" H 6105 5677 50  0000 C CNN
F 2 "" H 6100 5850 50  0001 C CNN
F 3 "" H 6100 5850 50  0001 C CNN
	1    6100 5850
	1    0    0    -1  
$EndComp
$Comp
L Device:Crystal Y?
U 1 1 5FD5275D
P 4700 3750
AR Path="/5FD5275D" Ref="Y?"  Part="1" 
AR Path="/5FD409CD/5FD5275D" Ref="Y1"  Part="1" 
F 0 "Y1" H 4700 4018 50  0000 C CNN
F 1 "8MHz" H 4700 3927 50  0000 C CNN
F 2 "Crystal:Crystal_SMD_HC49-SD" H 4700 3750 50  0001 C CNN
F 3 "~" H 4700 3750 50  0001 C CNN
	1    4700 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5FD52763
P 4450 3950
AR Path="/5FD52763" Ref="C?"  Part="1" 
AR Path="/5FD409CD/5FD52763" Ref="C10"  Part="1" 
F 0 "C10" H 4565 3996 50  0000 L CNN
F 1 "18p" H 4565 3905 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 4488 3800 50  0001 C CNN
F 3 "~" H 4450 3950 50  0001 C CNN
	1    4450 3950
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5FD52769
P 4950 3950
AR Path="/5FD52769" Ref="C?"  Part="1" 
AR Path="/5FD409CD/5FD52769" Ref="C11"  Part="1" 
F 0 "C11" H 5065 3996 50  0000 L CNN
F 1 "18p" H 5065 3905 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 4988 3800 50  0001 C CNN
F 3 "~" H 4950 3950 50  0001 C CNN
	1    4950 3950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FD5276F
P 4450 4100
AR Path="/5FD5276F" Ref="#PWR?"  Part="1" 
AR Path="/5FD409CD/5FD5276F" Ref="#PWR012"  Part="1" 
F 0 "#PWR012" H 4450 3850 50  0001 C CNN
F 1 "GND" H 4455 3927 50  0000 C CNN
F 2 "" H 4450 4100 50  0001 C CNN
F 3 "" H 4450 4100 50  0001 C CNN
	1    4450 4100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FD52775
P 4950 4100
AR Path="/5FD52775" Ref="#PWR?"  Part="1" 
AR Path="/5FD409CD/5FD52775" Ref="#PWR013"  Part="1" 
F 0 "#PWR013" H 4950 3850 50  0001 C CNN
F 1 "GND" H 4955 3927 50  0000 C CNN
F 2 "" H 4950 4100 50  0001 C CNN
F 3 "" H 4950 4100 50  0001 C CNN
	1    4950 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 3750 4450 3750
Wire Wire Line
	4450 3750 4450 3800
Wire Wire Line
	4850 3750 4950 3750
Wire Wire Line
	4950 3750 4950 3800
Wire Wire Line
	4450 3750 4450 3400
Connection ~ 4450 3750
Wire Wire Line
	4950 3750 4950 3500
Connection ~ 4950 3750
Wire Wire Line
	6400 5850 6400 5800
Wire Wire Line
	6300 5800 6300 5850
Wire Wire Line
	6200 5800 6200 5850
Wire Wire Line
	6100 5800 6100 5850
Wire Wire Line
	6400 2800 6400 2700
Wire Wire Line
	6300 2700 6300 2800
Wire Wire Line
	6200 2800 6200 2700
Wire Wire Line
	6100 2700 6100 2800
Wire Wire Line
	4950 3500 5600 3500
Wire Wire Line
	4450 3400 5600 3400
Wire Wire Line
	4100 2750 4100 2600
Connection ~ 4100 3200
Wire Wire Line
	4100 3050 4100 3200
$Comp
L power:+3.3V #PWR?
U 1 1 5FD52796
P 4100 2600
AR Path="/5FD52796" Ref="#PWR?"  Part="1" 
AR Path="/5FD409CD/5FD52796" Ref="#PWR07"  Part="1" 
F 0 "#PWR07" H 4100 2450 50  0001 C CNN
F 1 "+3.3V" H 4115 2773 50  0000 C CNN
F 2 "" H 4100 2600 50  0001 C CNN
F 3 "" H 4100 2600 50  0001 C CNN
	1    4100 2600
	1    0    0    -1  
$EndComp
$Comp
L Jumper:SolderJumper_2_Open JP?
U 1 1 5FD5279C
P 4100 2900
AR Path="/5FD5279C" Ref="JP?"  Part="1" 
AR Path="/5FD409CD/5FD5279C" Ref="JP1"  Part="1" 
F 0 "JP1" V 4100 2650 50  0000 L CNN
F 1 "SolderJumper_2_Open" V 4200 2000 50  0000 L CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 4100 2900 50  0001 C CNN
F 3 "~" H 4100 2900 50  0001 C CNN
	1    4100 2900
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FD527A2
P 4100 3650
AR Path="/5FD527A2" Ref="#PWR?"  Part="1" 
AR Path="/5FD409CD/5FD527A2" Ref="#PWR010"  Part="1" 
F 0 "#PWR010" H 4100 3400 50  0001 C CNN
F 1 "GND" H 4105 3477 50  0000 C CNN
F 2 "" H 4100 3650 50  0001 C CNN
F 3 "" H 4100 3650 50  0001 C CNN
	1    4100 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 3200 4100 3350
Wire Wire Line
	5600 3200 4100 3200
$Comp
L Device:R R?
U 1 1 5FD527AA
P 4100 3500
AR Path="/5FD527AA" Ref="R?"  Part="1" 
AR Path="/5FD409CD/5FD527AA" Ref="R2"  Part="1" 
F 0 "R2" H 4170 3546 50  0000 L CNN
F 1 "10k" H 4170 3455 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 4030 3500 50  0001 C CNN
F 3 "~" H 4100 3500 50  0001 C CNN
	1    4100 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	8050 2550 7950 2550
Wire Wire Line
	5600 3000 5550 3000
$Comp
L MCU_ST_STM32F3:STM32F334C8Tx U?
U 1 1 5FD527B2
P 6300 4300
AR Path="/5FD527B2" Ref="U?"  Part="1" 
AR Path="/5FD409CD/5FD527B2" Ref="U1"  Part="1" 
F 0 "U1" H 6250 4450 50  0000 C CNN
F 1 "STM32F334C8Tx" H 6250 4550 50  0000 C CNN
F 2 "Package_QFP:LQFP-48_7x7mm_P0.5mm" H 5700 2900 50  0001 R CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/DM00097745.pdf" H 6300 4300 50  0001 C CNN
	1    6300 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 3000 5550 2450
$Comp
L power:GND #PWR?
U 1 1 5FD527BB
P 7950 2600
AR Path="/5FD527BB" Ref="#PWR?"  Part="1" 
AR Path="/5FD409CD/5FD527BB" Ref="#PWR08"  Part="1" 
F 0 "#PWR08" H 7950 2350 50  0001 C CNN
F 1 "GND" H 7955 2427 50  0000 C CNN
F 2 "" H 7950 2600 50  0001 C CNN
F 3 "" H 7950 2600 50  0001 C CNN
	1    7950 2600
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FD527C1
P 5100 2900
AR Path="/5FD527C1" Ref="#PWR?"  Part="1" 
AR Path="/5FD409CD/5FD527C1" Ref="#PWR09"  Part="1" 
F 0 "#PWR09" H 5100 2650 50  0001 C CNN
F 1 "GND" H 5105 2727 50  0000 C CNN
F 2 "" H 5100 2900 50  0001 C CNN
F 3 "" H 5100 2900 50  0001 C CNN
	1    5100 2900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5FD527C7
P 5100 2650
AR Path="/5FD527C7" Ref="C?"  Part="1" 
AR Path="/5FD409CD/5FD527C7" Ref="C9"  Part="1" 
F 0 "C9" H 5215 2696 50  0000 L CNN
F 1 "100n" H 5215 2605 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 5138 2500 50  0001 C CNN
F 3 "~" H 5100 2650 50  0001 C CNN
	1    5100 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	7950 2550 7950 2600
Wire Wire Line
	5100 2800 5100 2900
Wire Wire Line
	5100 2500 5100 2450
Connection ~ 6100 5850
Wire Wire Line
	5600 5300 5350 5300
Wire Wire Line
	5600 5400 5350 5400
Wire Wire Line
	5600 5500 5350 5500
Wire Wire Line
	5600 5600 5350 5600
Wire Wire Line
	7200 4600 6900 4600
Wire Wire Line
	7200 4700 6900 4700
Wire Wire Line
	7200 4800 6900 4800
$Comp
L Device:R R8
U 1 1 5FD963E5
P 8200 5000
F 0 "R8" H 8270 5046 50  0000 L CNN
F 1 "1k" H 8270 4955 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 8130 5000 50  0001 C CNN
F 3 "~" H 8200 5000 50  0001 C CNN
	1    8200 5000
	-1   0    0    -1  
$EndComp
$Comp
L Device:LED D3
U 1 1 5FD97B40
P 8200 4700
F 0 "D3" V 8239 4582 50  0000 R CNN
F 1 "LED" V 8148 4582 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 8200 4700 50  0001 C CNN
F 3 "~" H 8200 4700 50  0001 C CNN
	1    8200 4700
	0    1    -1   0   
$EndComp
Wire Wire Line
	8200 4500 8200 4550
$Comp
L power:GND #PWR019
U 1 1 5FD9D72D
P 8200 5150
F 0 "#PWR019" H 8200 4900 50  0001 C CNN
F 1 "GND" H 8205 4977 50  0000 C CNN
F 2 "" H 8200 5150 50  0001 C CNN
F 3 "" H 8200 5150 50  0001 C CNN
	1    8200 5150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6500 2700 6500 2800
Wire Wire Line
	8200 4500 6900 4500
$Comp
L Connector:Conn_01x04_Male J8
U 1 1 5FDBD411
P 8650 5600
F 0 "J8" H 8600 5450 50  0000 C CNN
F 1 "SWD Connector" H 8350 5550 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 8650 5600 50  0001 C CNN
F 3 "~" H 8650 5600 50  0001 C CNN
	1    8650 5600
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6900 5500 8450 5500
Wire Wire Line
	8450 5700 8050 5700
Wire Wire Line
	8050 5700 8050 5400
Wire Wire Line
	8050 5400 6900 5400
$Comp
L power:GND #PWR017
U 1 1 5FDCEA79
P 7900 5600
F 0 "#PWR017" H 7900 5350 50  0001 C CNN
F 1 "GND" H 7905 5427 50  0000 C CNN
F 2 "" H 7900 5600 50  0001 C CNN
F 3 "" H 7900 5600 50  0001 C CNN
	1    7900 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	8450 5600 7900 5600
Wire Wire Line
	8450 5800 7700 5800
Wire Wire Line
	7700 2450 5550 2450
Connection ~ 5550 2450
$Comp
L Device:R R1
U 1 1 5FDDF76B
P 5100 2300
F 0 "R1" H 5170 2346 50  0000 L CNN
F 1 "10k" H 5170 2255 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 5030 2300 50  0001 C CNN
F 3 "~" H 5100 2300 50  0001 C CNN
	1    5100 2300
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR06
U 1 1 5FDE09FF
P 5100 2150
F 0 "#PWR06" H 5100 2000 50  0001 C CNN
F 1 "+3.3V" H 5115 2323 50  0000 C CNN
F 2 "" H 5100 2150 50  0001 C CNN
F 3 "" H 5100 2150 50  0001 C CNN
	1    5100 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 4100 5600 4100
Wire Wire Line
	5600 4200 5550 4200
Wire Wire Line
	3300 4400 3300 4450
Wire Wire Line
	3050 4400 3300 4400
Wire Wire Line
	3200 3900 3200 4000
Wire Wire Line
	3200 3700 3200 3800
$Comp
L Device:D_Schottky D1
U 1 1 5FDA752D
P 3200 4150
F 0 "D1" V 3154 4230 50  0000 L CNN
F 1 "D_Schottky" V 3245 4230 50  0000 L CNN
F 2 "Diode_SMD:D_SOD-123F" H 3200 4150 50  0001 C CNN
F 3 "~" H 3200 4150 50  0001 C CNN
	1    3200 4150
	0    -1   1    0   
$EndComp
$Comp
L Connector:Conn_01x02_Male J6
U 1 1 5FD9D731
P 3000 3900
F 0 "J6" H 2972 3782 50  0000 R CNN
F 1 "Conn_01x02_Male" H 2972 3873 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 3000 3900 50  0001 C CNN
F 3 "~" H 3000 3900 50  0001 C CNN
	1    3000 3900
	1    0    0    1   
$EndComp
Wire Wire Line
	3200 4600 3050 4600
Wire Wire Line
	3200 4300 3200 4600
$Comp
L power:+5V #PWR011
U 1 1 5FE616D5
P 3200 3700
F 0 "#PWR011" H 3200 3550 50  0001 C CNN
F 1 "+5V" H 3215 3873 50  0000 C CNN
F 2 "" H 3200 3700 50  0001 C CNN
F 3 "" H 3200 3700 50  0001 C CNN
	1    3200 3700
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR015
U 1 1 5FD790BF
P 3300 4450
F 0 "#PWR015" H 3300 4200 50  0001 C CNN
F 1 "GND" H 3305 4277 50  0000 C CNN
F 2 "" H 3300 4450 50  0001 C CNN
F 3 "" H 3300 4450 50  0001 C CNN
	1    3300 4450
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x06_Male J7
U 1 1 5FD63893
P 2850 4600
F 0 "J7" H 2822 4574 50  0000 R CNN
F 1 "FTDI Connector" H 2822 4483 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 2850 4600 50  0001 C CNN
F 3 "~" H 2850 4600 50  0001 C CNN
	1    2850 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 5200 6900 5200
Wire Wire Line
	6900 5100 7200 5100
NoConn ~ 3050 4500
NoConn ~ 3050 4900
Wire Wire Line
	7200 5300 6900 5300
Text HLabel 5550 3000 0    50   Input ~ 0
NRST
Text HLabel 5350 5600 0    50   Input ~ 0
HRTIM1_CHD2
Text HLabel 5350 5500 0    50   Input ~ 0
HRTIM1_CHD1
Text HLabel 7200 5300 2    50   Input ~ 0
PA12
Text HLabel 7200 5200 2    50   Input ~ 0
PA11
Text HLabel 7200 5000 2    50   Input ~ 0
PA9
Text HLabel 7200 4900 2    50   Input ~ 0
PA8
Text HLabel 7200 4600 2    50   Input ~ 0
SPI1_SCK
Text HLabel 7200 5100 2    50   Input ~ 0
PA10
Text HLabel 5550 4300 0    50   Input ~ 0
PB2
Wire Wire Line
	5550 4300 5600 4300
$Comp
L Device:R R7
U 1 1 6020592A
P 8600 5000
F 0 "R7" H 8670 5046 50  0000 L CNN
F 1 "1k" H 8670 4955 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 8530 5000 50  0001 C CNN
F 3 "~" H 8600 5000 50  0001 C CNN
	1    8600 5000
	-1   0    0    -1  
$EndComp
$Comp
L Device:LED D2
U 1 1 60205930
P 8600 4700
F 0 "D2" V 8639 4582 50  0000 R CNN
F 1 "LED" V 8548 4582 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 8600 4700 50  0001 C CNN
F 3 "~" H 8600 4700 50  0001 C CNN
	1    8600 4700
	0    1    -1   0   
$EndComp
$Comp
L power:GND #PWR018
U 1 1 60205936
P 8600 5150
F 0 "#PWR018" H 8600 4900 50  0001 C CNN
F 1 "GND" H 8605 4977 50  0000 C CNN
F 2 "" H 8600 5150 50  0001 C CNN
F 3 "" H 8600 5150 50  0001 C CNN
	1    8600 5150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8600 4550 8600 4400
Wire Wire Line
	8600 4400 6900 4400
NoConn ~ 5600 3700
NoConn ~ 5600 3800
NoConn ~ 5600 3900
NoConn ~ 5600 4400
NoConn ~ 5600 4500
NoConn ~ 5600 4600
NoConn ~ 5600 4900
NoConn ~ 5600 5000
NoConn ~ 6900 5600
NoConn ~ 6900 4100
$Comp
L Connector:Conn_01x02_Male J5
U 1 1 601ABDF0
P 8250 2450
F 0 "J5" H 8222 2332 50  0000 R CNN
F 1 "Reset" H 8222 2423 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 8250 2450 50  0001 C CNN
F 3 "~" H 8250 2450 50  0001 C CNN
	1    8250 2450
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7700 2450 8050 2450
Connection ~ 7700 2450
Wire Wire Line
	5100 2450 5550 2450
Connection ~ 5100 2450
Text HLabel 7200 4700 2    50   Input ~ 0
SPI1_MISO
Text HLabel 7200 4800 2    50   Input ~ 0
SPI1_MOSI
Wire Wire Line
	6900 4200 7200 4200
Wire Wire Line
	6900 4300 7200 4300
Wire Wire Line
	6900 4900 7200 4900
Wire Wire Line
	7200 5000 6900 5000
Text HLabel 5550 4100 0    50   Input ~ 0
ADC1_IN11
Text HLabel 5550 4200 0    50   Input ~ 0
ADC1_IN12
Text HLabel 7200 4300 2    50   Input ~ 0
ADC1_IN3
Text HLabel 7200 4200 2    50   Input ~ 0
ADC1_IN2
NoConn ~ 5600 5100
NoConn ~ 5600 5200
Text HLabel 5350 5300 0    50   Input ~ 0
HRTIM1_CHC1
Text HLabel 5350 5400 0    50   Input ~ 0
HRTIM1_CHC2
Wire Wire Line
	6500 2700 7600 2700
Wire Wire Line
	3050 4800 4000 4800
Wire Wire Line
	3050 4700 3600 4700
$Comp
L Device:R R3
U 1 1 606732F8
P 3750 4700
F 0 "R3" V 3543 4700 50  0000 C CNN
F 1 "1k" V 3634 4700 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 3680 4700 50  0001 C CNN
F 3 "~" H 3750 4700 50  0001 C CNN
	1    3750 4700
	0    1    1    0   
$EndComp
Wire Wire Line
	3900 4700 5600 4700
$Comp
L Device:R R4
U 1 1 60673DA1
P 4150 4800
F 0 "R4" V 4350 4800 50  0000 C CNN
F 1 "1k" V 4250 4800 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 4080 4800 50  0001 C CNN
F 3 "~" H 4150 4800 50  0001 C CNN
	1    4150 4800
	0    1    1    0   
$EndComp
Wire Wire Line
	4300 4800 5600 4800
Wire Wire Line
	7700 5800 7700 2450
Connection ~ 7250 1550
Wire Wire Line
	7600 1550 7250 1550
Connection ~ 6550 1550
Wire Wire Line
	6450 1550 6550 1550
$Comp
L Device:C C6
U 1 1 5FD9E07B
P 6550 1700
F 0 "C6" H 6665 1746 50  0000 L CNN
F 1 "10n" H 6665 1655 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 6588 1550 50  0001 C CNN
F 3 "~" H 6550 1700 50  0001 C CNN
	1    6550 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	6550 1550 6900 1550
Connection ~ 6900 1550
$Comp
L Device:L L8
U 1 1 60652422
P 6300 1550
F 0 "L8" V 6490 1550 50  0000 C CNN
F 1 "100u" V 6399 1550 50  0000 C CNN
F 2 "Inductor_SMD:L_1210_3225Metric_Pad1.42x2.65mm_HandSolder" H 6300 1550 50  0001 C CNN
F 3 "~" H 6300 1550 50  0001 C CNN
	1    6300 1550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6900 1850 7250 1850
$Comp
L Device:C C8
U 1 1 6019DE38
P 7250 1700
F 0 "C8" H 7365 1746 50  0000 L CNN
F 1 "100n" H 7365 1655 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 7288 1550 50  0001 C CNN
F 3 "~" H 7250 1700 50  0001 C CNN
	1    7250 1700
	1    0    0    -1  
$EndComp
Connection ~ 6900 1850
$Comp
L power:GND #PWR05
U 1 1 5FDA2712
P 6900 1850
F 0 "#PWR05" H 6900 1600 50  0001 C CNN
F 1 "GND" H 6905 1677 50  0000 C CNN
F 2 "" H 6900 1850 50  0001 C CNN
F 3 "" H 6900 1850 50  0001 C CNN
	1    6900 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	6550 1850 6900 1850
Wire Wire Line
	6900 1550 7250 1550
$Comp
L Device:C C7
U 1 1 5FD9EA8E
P 6900 1700
F 0 "C7" H 7015 1746 50  0000 L CNN
F 1 "1u" H 7015 1655 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 6938 1550 50  0001 C CNN
F 3 "~" H 6900 1700 50  0001 C CNN
	1    6900 1700
	1    0    0    -1  
$EndComp
Connection ~ 5800 1550
Connection ~ 4400 1850
$Comp
L power:GND #PWR?
U 1 1 5FD52806
P 4400 1850
AR Path="/5FD52806" Ref="#PWR?"  Part="1" 
AR Path="/5FD409CD/5FD52806" Ref="#PWR04"  Part="1" 
F 0 "#PWR04" H 4400 1600 50  0001 C CNN
F 1 "GND" H 4405 1677 50  0000 C CNN
F 2 "" H 4400 1850 50  0001 C CNN
F 3 "" H 4400 1850 50  0001 C CNN
	1    4400 1850
	1    0    0    -1  
$EndComp
Connection ~ 4400 1550
Wire Wire Line
	5450 1550 5800 1550
Connection ~ 5450 1550
Wire Wire Line
	5100 1550 5450 1550
Connection ~ 5100 1550
Wire Wire Line
	4750 1550 5100 1550
Connection ~ 4750 1550
Wire Wire Line
	4400 1550 4750 1550
Wire Wire Line
	5450 1850 5800 1850
Connection ~ 5450 1850
Wire Wire Line
	5100 1850 5450 1850
Connection ~ 5100 1850
Wire Wire Line
	4750 1850 5100 1850
Connection ~ 4750 1850
Wire Wire Line
	4400 1850 4750 1850
$Comp
L Device:C C?
U 1 1 5FD527F1
P 4400 1700
AR Path="/5FD527F1" Ref="C?"  Part="1" 
AR Path="/5FD409CD/5FD527F1" Ref="C1"  Part="1" 
F 0 "C1" H 4515 1746 50  0000 L CNN
F 1 "10u" H 4515 1655 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 4438 1550 50  0001 C CNN
F 3 "~" H 4400 1700 50  0001 C CNN
	1    4400 1700
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5FD527EB
P 4750 1700
AR Path="/5FD527EB" Ref="C?"  Part="1" 
AR Path="/5FD409CD/5FD527EB" Ref="C2"  Part="1" 
F 0 "C2" H 4865 1746 50  0000 L CNN
F 1 "100n" H 4865 1655 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 4788 1550 50  0001 C CNN
F 3 "~" H 4750 1700 50  0001 C CNN
	1    4750 1700
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5FD527E5
P 5100 1700
AR Path="/5FD527E5" Ref="C?"  Part="1" 
AR Path="/5FD409CD/5FD527E5" Ref="C3"  Part="1" 
F 0 "C3" H 5215 1746 50  0000 L CNN
F 1 "100n" H 5215 1655 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 5138 1550 50  0001 C CNN
F 3 "~" H 5100 1700 50  0001 C CNN
	1    5100 1700
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5FD527DF
P 5450 1700
AR Path="/5FD527DF" Ref="C?"  Part="1" 
AR Path="/5FD409CD/5FD527DF" Ref="C4"  Part="1" 
F 0 "C4" H 5565 1746 50  0000 L CNN
F 1 "100n" H 5565 1655 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 5488 1550 50  0001 C CNN
F 3 "~" H 5450 1700 50  0001 C CNN
	1    5450 1700
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5FD527D9
P 5800 1700
AR Path="/5FD527D9" Ref="C?"  Part="1" 
AR Path="/5FD409CD/5FD527D9" Ref="C5"  Part="1" 
F 0 "C5" H 5915 1746 50  0000 L CNN
F 1 "100n" H 5915 1655 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 5838 1550 50  0001 C CNN
F 3 "~" H 5800 1700 50  0001 C CNN
	1    5800 1700
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5FD527D3
P 4400 1550
AR Path="/5FD527D3" Ref="#PWR?"  Part="1" 
AR Path="/5FD409CD/5FD527D3" Ref="#PWR03"  Part="1" 
F 0 "#PWR03" H 4400 1400 50  0001 C CNN
F 1 "+3.3V" H 4415 1723 50  0000 C CNN
F 2 "" H 4400 1550 50  0001 C CNN
F 3 "" H 4400 1550 50  0001 C CNN
	1    4400 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 1550 6100 1550
Wire Wire Line
	6100 2700 6100 1550
Connection ~ 6100 2700
Connection ~ 6100 1550
Wire Wire Line
	6100 1550 6150 1550
Wire Wire Line
	7600 2700 7600 1550
$EndSCHEMATC
