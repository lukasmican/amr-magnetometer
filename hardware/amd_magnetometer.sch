EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 9 12
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L AMR_1D_magnetometer-rescue:HMC1021S-Sensor_Magnetic U28
U 1 1 5FF2CB1D
P 5600 3800
F 0 "U28" H 5600 4215 50  0000 C CNN
F 1 "HMC1021S" H 5600 4124 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 5600 4100 50  0001 C CNN
F 3 "" H 5600 4100 50  0001 C CNN
	1    5600 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 3650 6450 3650
Wire Wire Line
	6100 3750 6450 3750
Wire Wire Line
	6100 3850 6450 3850
Wire Wire Line
	6450 3950 6100 3950
Text HLabel 6450 3950 2    50   Input ~ 0
SR_COIL+
Text HLabel 6450 3850 2    50   Input ~ 0
SR_COIL-
Text HLabel 6450 3650 2    50   Input ~ 0
OFFSET_COIL-
Text HLabel 6450 3750 2    50   Input ~ 0
OFFSET_COIL+
Wire Wire Line
	4650 3750 5100 3750
Wire Wire Line
	5100 3850 5050 3850
Wire Wire Line
	4650 3950 4750 3950
Wire Wire Line
	4650 3650 5100 3650
Text HLabel 4650 3650 0    50   Input ~ 0
OUT+
Text HLabel 4650 3950 0    50   Input ~ 0
OUT-
Text HLabel 4650 3750 0    50   Input ~ 0
V_BRIDGE
Text HLabel 4650 3850 0    50   Input ~ 0
GND
$Comp
L Device:R Rx
U 1 1 612BC6F4
P 4900 4150
F 0 "Rx" V 4800 4150 50  0000 C CNN
F 1 "2M" V 4900 4150 50  0000 C CNN
F 2 "" V 4830 4150 50  0001 C CNN
F 3 "~" H 4900 4150 50  0001 C CNN
	1    4900 4150
	0    1    1    0   
$EndComp
Connection ~ 4750 3950
Wire Wire Line
	4750 3950 5100 3950
Connection ~ 5050 3850
Wire Wire Line
	5050 3850 4650 3850
Wire Wire Line
	4750 4150 4750 3950
Wire Wire Line
	5050 4150 5050 3850
$EndSCHEMATC
